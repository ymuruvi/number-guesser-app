import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:numguesser/widgets/drawing_board.dart';
import 'package:numguesser/tools/net_handler.dart';

class IconBoard extends StatefulWidget {

  final double width;
  final double sliderMin;
  final double sliderMax;
  final parent;

  final DrawingBoard drawingBoard;
  late final NetHandler netHandler;

  IconBoard(
      {required this.width,
      required this.sliderMin,
      required this.sliderMax,
      required this.drawingBoard,
      required this.parent})
      : super(){
        this.netHandler = NetHandler(labelPath: 'assets\labels.txt', modelPath: 'assets\mnist_model.tflite');
      }

  @override
  _IconBoardState createState() => _IconBoardState();

}

class _IconBoardState extends State<IconBoard>  {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width * 0.8,
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        IconButton(
            icon: Icon(Icons.insights_rounded),
            onPressed: () async {
              this.setState(() {});
            }),
        Expanded(
            child: Row(
          children: [
            Text("#${widget.netHandler.currentGuess}"),
            Slider(
                min: widget.sliderMin,
                max: widget.sliderMax,
                activeColor: Colors.black,
                value: widget.drawingBoard.strokeWidth,
                onChanged: (value) {
                  this.setState(() {
                    widget.drawingBoard.strokeWidth = value;
                  });
                }),
            
          ],
        )),
        IconButton(
            icon: Icon(Icons.clear_all),
            onPressed: () async {
              widget.drawingBoard.clearPoints();
              widget.parent.update();
              this.setState(() {});
            })
      ]),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
    );
  }

  void update() {
    this.setState(() {});
  }
}
