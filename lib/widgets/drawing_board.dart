import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:numguesser/tools/helpers.dart';

class DrawingBoard extends StatefulWidget {

  final points;
  final scrnWidth;
  final scrnHeight;

  double strokeWidth = 30.0;

  DrawingBoard(
      {required this.points,
      required this.scrnWidth,
      required this.scrnHeight})
      : super();

  @override
  _DrawingBoardState createState() => _DrawingBoardState();

  void clearPoints(){
    this.points.clear();
  }

}

class _DrawingBoardState extends State<DrawingBoard> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              child: CustomPaint(
                painter: MyCustomPainter(points: widget.points),
              )),
          onPanDown: (details) {
            this.setState(() {
              Point tmp = Point.fromOffsetWithStroke(
                  details.localPosition, widget.strokeWidth);
              tmp.setStroke(widget.strokeWidth);
              widget.points.add(tmp);
            });
          },
          onPanUpdate: (details) {
            this.setState(() {
              Point tmp = Point.fromOffsetWithStroke(
                  details.localPosition, widget.strokeWidth);
              tmp.setStroke(widget.strokeWidth);
              widget.points.add(tmp);
            });
          },
          onPanEnd: (details) {
            this.setState(() {
              widget.points.add(EMPTY_OFFSET);
            });
          }),
      width: widget.scrnWidth * 0.9,
      height: widget.scrnWidth * 0.9,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.4),
                blurRadius: 5.0,
                spreadRadius: 1.0)
          ]),
    );
  }

  void update(){
    this.setState(() {
    });
  }

}

class MyCustomPainter extends CustomPainter {

  List<Point> points;
  MyCustomPainter({required this.points});

  @override
  Future<void> paint(Canvas canvas, Size size) async {

    Paint blk = Paint()..color = Colors.black;
    Rect rect = Rect.fromLTRB(0, 0, size.width, size.height);
    Paint paint = Paint();
    Point prevP = EMPTY_OFFSET;

    canvas.drawRect(rect, blk);

    paint.color = Colors.white;
    paint.isAntiAlias = true;
    paint.strokeCap = StrokeCap.round;

    for (Point p in points) {
      if (prevP == EMPTY_OFFSET) {
        paint.strokeWidth = p.getStroke();
        canvas.drawPoints(PointMode.points, [p], paint);
      } else if (p != EMPTY_OFFSET) {
        canvas.drawLine(prevP, p, paint);
      }
      prevP = p;
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return (this != oldDelegate);
  }
}
