
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'states/homepage_state.dart';
import 'tools/helpers.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Drawing App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //App portion
      home: MyHomePage(
        title: 'Number Drawing App'
      ),
    );

  }

}

