import 'dart:ui';
import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../widgets/drawing_board.dart';
import '../tools/helpers.dart';
import '../widgets/icon_board.dart';
import '../tools/widget_converter.dart';

class MyHomePage extends StatefulWidget {

  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
  
}

class _MyHomePageState extends State<MyHomePage> {

  GlobalKey _globalKey = new GlobalKey();
  GlobalKey imgKey = new GlobalKey();

  late DrawingBoard drawingBoard;
  late IconBoard iconBoard;

  late Uint8List imageBytes;

  List<Point> points = [];
  late Color selectedColor;
  var messageText;

  static const PIXEL_RATIO = 3.0;

  @override
  void initState() {
    super.initState();
    selectedColor = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;

    DrawingBoard drawingBoard = DrawingBoard(
      points: points,
      scrnHeight: height,
      scrnWidth: width
    );

    IconBoard iconBoard = IconBoard(
      width: width,
      sliderMin: 15.0,
      sliderMax: 50.0,
      drawingBoard: drawingBoard, 
      parent: this, 
    );

    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.blueGrey,
          ),
        ),
        Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            WidgetConverter(
              builder: (key) {
                this.imgKey = key;
                return drawingBoard;
              },
            ),
            SizedBox(
              height: 20,
            ),
            iconBoard
          ],
        ))
      ],
    )
    );
  }

  void update() => this.setState(() {});
  
  // ignore: unused_element
  Future<Uint8List> _captureImg() async {
    var imgBytes;
    try {
      RenderRepaintBoundary boundary = _globalKey.currentContext!
          .findRenderObject() as RenderRepaintBoundary;
      Image img = (await boundary.toImage(pixelRatio: PIXEL_RATIO)) as Image;
    } catch (e) {
      //Handle exception
      print(e);
    }
    return imgBytes;
  }
}
