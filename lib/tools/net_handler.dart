


class NetHandler {
  late String currentGuess;

  late Future<String?> res;

  NetHandler({required String labelPath, required String modelPath}) {
    this.currentGuess = "?";
  }

  Future<void> makeGuess() async {
    this.currentGuess = "?";
  }
}
