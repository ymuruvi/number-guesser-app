
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tflite/tflite.dart';

Point EMPTY_OFFSET = Point(0, 0);

class GuessHandler {
  var _guess;

  GuessHandler() {
    this._guess = 'None';
  }

  void makeGuess(var input) {
    if (input == null) {}
  }

  String getGuess() {
    return (this._guess == null) ? 'None' : '$this._guess';
  }
}

class Point extends Offset{
  late Color _col;
  late double _stroke;

  @override
  Point(double dx, double dy) : super(dx, dy){
    this._col = Colors.black;
    this._stroke = 3.0;
  }

  Point.fromOffset(Offset alt): super(alt.dx, alt.dy){
    Point(alt.dx, alt.dy);
  }

  Point.fromOffsetWithStroke(Offset alt, double s): super(alt.dx, alt.dy){
    this._stroke = s;
  }

  void setColor(Color c) => this._col = c;

  void setStroke(double stroke) => this._stroke = stroke;

  Color getColor() => this._col;

  double getStroke() => this._stroke;

}

class Utils{

  static captureImg(GlobalKey key) async{
    if(key == null) return null;

    RenderRepaintBoundary? boundary = key.currentContext!.findRenderObject() as RenderRepaintBoundary?;

    final img = await boundary!.toImage(pixelRatio: 3);
    final byteData = await img.toByteData(format: ImageByteFormat.png);
    final pngBytes = byteData!.buffer.asUint8List();

    return pngBytes;
  }

}