
import 'package:flutter/material.dart';

class WidgetConverter extends StatefulWidget{

  final Function(GlobalKey key) builder;

  const WidgetConverter({
    required this.builder,
    Key? key,
  }): super(key: key);

  @override
  _WidgetConverterState createState() => _WidgetConverterState();
  
}

class _WidgetConverterState extends State<WidgetConverter>{

  final _globalKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: widget.builder(_globalKey),
    );
  }
  
}
