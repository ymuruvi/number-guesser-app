import 'dart:ui';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'tools/helpers.dart';
import 'tools/widget_converter.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey _globalKey = new GlobalKey();

  List<Point> points = [];
  late Color selectedColor;
  late double strokeWidth;
  GuessHandler gh = GuessHandler();
  var messageText;

  static const PIXEL_RATIO = 3.0;

  @override
  void initState() {
    super.initState();
    selectedColor = Colors.white;
    strokeWidth = 30.0;
    gh = GuessHandler();
  }

  @override
  Widget build(BuildContext context) {
    final double WIDTH = MediaQuery.of(context).size.width;
    final double HEIGHT = WIDTH;

    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.blueGrey,
          ),
        ),
        Center(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: GestureDetector(
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: CustomPaint(
                        painter: MyCustomPainter(points: points),
                      )
                    ),
                  onPanDown: (details) {
                    this.setState(() {
                      Point tmp = Point.fromOffsetWithStroke(
                          details.localPosition, strokeWidth);
                      tmp.setStroke(strokeWidth);
                      points.add(tmp);
                    });
                  },
                  onPanUpdate: (details) {
                    this.setState(() {
                      Point tmp = Point.fromOffsetWithStroke(
                          details.localPosition, strokeWidth);
                      tmp.setStroke(strokeWidth);
                      points.add(tmp);
                    });
                  },
                  onPanEnd: (details) {
                    this.setState(() {
                      points.add(EMPTY_OFFSET);
                    });
                  }),
              width: WIDTH * 0.9,
              height: HEIGHT * 0.9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.4),
                        blurRadius: 5.0,
                        spreadRadius: 1.0)
                  ]),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: WIDTH * 0.8,
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                IconButton(
                    icon: Icon(Icons.insights_rounded),
                    onPressed: () {
                      gh.makeGuess(null);
                      this.setState(() {
                        messageText = gh.getGuess();
                      });
                    }),
                Expanded(
                    child: Row(
                  children: [
                    Slider(
                        min: 20.0,
                        max: 50.0,
                        activeColor: Colors.black,
                        value: strokeWidth,
                        onChanged: (value) {
                          this.setState(() {
                            strokeWidth = value;
                          });
                        }),
                  ],
                )),
                IconButton(
                    icon: Icon(Icons.clear_all),
                    onPressed: () {
                      points.clear();
                      this.setState(() {
                        messageText = 'Cleared';
                      });
                    })
              ]),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
            )
          ],
        ))
      ],
    ));
  }

  // ignore: unused_element
  Future<Uint8List> _captureImg() async {
    var imgBytes;
    try {
      RenderRepaintBoundary boundary = _globalKey.currentContext!
          .findRenderObject() as RenderRepaintBoundary;
      Image img = (await boundary.toImage(pixelRatio: PIXEL_RATIO)) as Image;
    } catch (e) {
      //Handle exception
      print(e);
    }
    return imgBytes;
  }
}

class MyCustomPainter extends CustomPainter {
  double stroke_width = 10.0;

  List<Point> points = [];

  MyCustomPainter({required this.points});

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    Paint bk = Paint()..color = Colors.black;
    Rect rect = Rect.fromLTRB(0, 0, size.width, size.height);
    Paint paint = Paint();
    Point prevP = EMPTY_OFFSET;

    //draw background
    canvas.drawRect(rect, bk);

    //Drawing settings
    paint.color = Colors.white;
    paint.isAntiAlias = true;
    paint.strokeCap = StrokeCap.round;

    for (Point p in points) {
      if (prevP == EMPTY_OFFSET) {
        paint.strokeWidth = p.getStroke();
        canvas.drawPoints(PointMode.points, [p], paint);
      } else if (p != EMPTY_OFFSET) {
        canvas.drawLine(prevP, p, paint);
      }
      prevP = p;
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return (this != oldDelegate);
  }
}
