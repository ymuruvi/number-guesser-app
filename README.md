# Number Guesser

Flutter project to test my mnist model.

It uses a TFlite model developed using the MNIST library to take an image drawn in the app and make a guess on what number was drawn.
